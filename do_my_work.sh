#!/bin/sh

delay(){
  # Wait a single char
  read  -n 1
  echo -e "\n\n"
}

server_id=$1
router_mac=$2
green="\033[32m"
magenta="\033[35m"
reset="\033[39m"

echo "Liste de toutes les instances avec libvirt"
printf "$green# virsh list$reset\n"
sudo virsh list
delay

echo "Récupération du nom de notre instance grâce à l'uid"
printf "$green# virsh dumpxml {instance} | grep $server_id $reset\n"
virsh_instance=$(sudo virsh list | tail -n +3 | head -n -1 | awk -F' ' '{print $2}' | while read line; do sudo virsh dumpxml $line | grep "$server_id" >/dev/null 2>&1 && echo $line; done)
printf "Instance : $magenta$virsh_instance$reset\n"
delay

echo "Lecture du tag interface dans le dump xml"
printf "$green# virsh dumpxml $virsh_instance | sed -n '/<interface/,/<\/interface>/p'$reset\n"
interfaces=$(sudo virsh dumpxml $virsh_instance | sed -n '/<interface/,/<\/interface>/p')
printf "$interfaces"
bridge=$(echo $interfaces | grep -oP "(?<=bridge=')[\w-]*")
dev=$(echo $interfaces | grep -oP "(?<=dev=')[\w-]*")
printf "\n\nBridge = $magenta$bridge$reset\n"
printf "Interface = $magenta$dev$reset\n"
delay

echo "Affichage des informations brctl"
printf "$green# brctl show | grep -A 1 $bridge$reset\n"
sudo brctl show | grep -A 1 "$bridge"
delay

echo "Recuperation des informations openvswitch"
printf "$green# ovs-vsctl show | grep $ovs_name$reset\n"
ovs_name="qvo${bridge:3}"
sudo ovs-vsctl show | head -n 11
out=$(sudo ovs-vsctl show | grep -A 1 "$ovs_name" | head -n 3)
printf "$magenta$out$reset\n"
delay

echo "Table de flux br-int"
printf "$green# ovs-ofctl dump-flows br-int --names --no-stats$reset\n"
all_lines=$(sudo ovs-ofctl dump-flows br-int --names --no-stats)
exclude_lines=$(sudo ovs-ofctl dump-flows br-int --names --no-stats | grep "in_port=" | grep -v "$ovs_name")
python -c "exclude_lines = '''${exclude_lines}'''; all_lines='''${all_lines}'''; format = lambda s, val : '\033[31m' + s if val else '\033[39m' + s; print('\n'.join([format(line, line in exclude_lines.split('\n')) for line in all_lines.split('\n')])); print(format('', False))"
delay


echo "Table de flux br-tun"
printf "$green# ovs-ofctl dump-flows br-tun --names --no-stats$reset\n"
all_lines=$(sudo ovs-ofctl dump-flows br-tun --names --no-stats)
exclude_lines=$(sudo ovs-ofctl dump-flows br-tun --names --no-stats | grep "in_port=\"vxlan\|table=4\|table=10\|table=6" )

python -c "exclude_lines = '''${exclude_lines}'''; all_lines='''${all_lines}'''; format = lambda s, val : '\033[31m' + s if val else '\033[39m' + s; print('\n'.join([format(line, line in exclude_lines.split('\n')) for line in all_lines.split('\n')])); print(format('', False))"
delay

file=dump_ping
echo "Capture tcpdump dans $file"
printf "$green# tcpdump -i em1 -w $file$reset\n"
sudo tcpdump -i em1 -w "$file"
